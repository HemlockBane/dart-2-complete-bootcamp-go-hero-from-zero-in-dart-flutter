void main(){
  var myDog = new Dog();
  myDog.breed = "Dobberman";
  myDog.eat();

  var myLion = new Lion();
  myLion.color = "GrayGold";
  myLion.age = 55;

  print(myLion.color);
  print(myLion.age);

}

class Animal{
  String color;
  String breed;
  void eat(){
    print("Eating");
  }
  void Sleep(){
    print("Sleeping");
  }
}

class Dog extends Animal{
 String breed;
}
class Lion extends Animal{
  int age;
}