
void main(){
  var myBike = new Bike.named();
  print(myBike.bikeName);
  print(myBike.color);
  print(myBike.variant);


  var myTopEnd = new Bike.topEnd();
  print(myTopEnd.bikeName);
  print(myTopEnd.color);
}

class Bike{
    String bikeName;
    String color;
    int variant;

    Bike(this.bikeName, this.color, this.variant);

  Bike.named(){
    bikeName = "Scott";
    color = "Blue, Red and White";
    variant = 2;
  }

  Bike.topEnd(){
    bikeName = "Top end";
    color ="Grey";
    variant = 3;
  }
}