void main(){

    var myBus = new Bus("Scania","Black and Yellow",6);
    var myBus2 = new Bus("Datcia","Red",12);

    print(myBus.busName);
    print(myBus.color);
    print(myBus.variant);
    print("");
    print(myBus2.busName);
    print(myBus2.color);
    print(myBus2.variant);

}

class Bus{
  String busName;
  String color;
  int variant;

  // Bus(this.busName, this.color, this.variant);
  // OR
  Bus(String busName, String color, int variant){
    this.busName = busName;
    this.color = color;
    this.variant = variant;
  }

}